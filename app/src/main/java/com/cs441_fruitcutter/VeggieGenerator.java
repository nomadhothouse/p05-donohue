package com.cs441_fruitcutter;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Region;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Created by Thomas on 2/27/2016.
 */
public class VeggieGenerator {

    enum VeggieType {
        CARROT(R.mipmap.carrot),
        POTATO(R.mipmap.potato),
        BROCCOLI(R.mipmap.broccoli),
        EGGPLANT(R.mipmap.eggplant),
        PEPPER(R.mipmap.pepper);

        private final int resourceId;

        private VeggieType(int resourceId) {
            this.resourceId = resourceId;
        }

        public int getResourceId() {
            return resourceId;
        }

        private static final Random random = new Random();

        public static VeggieType randomVeg() {
            return VeggieType.values()[random.nextInt(VeggieType.values().length)];
        }
    }

    private final Random random = new Random();
    private final List<Veggie> veggies = new ArrayList<Veggie>();

    // http://developer.android.com/reference/android/util/SparseArray.html
    // more memory efficient than using a HashMap to map Integers to Objects
    // avoids auto-boxing keys and its data structure doesn't rely on an extra entry object for each mapping
    private final SparseArray<Bitmap> bitmapImages;

    // http://developer.android.com/reference/android/graphics/Region.html
    private Region clip;
    private int maxWidth;
    private int maxHeight;

    public VeggieGenerator(Resources r) {
        // put all of the veggie images into a sparse array, associates integers to objects
        bitmapImages = new SparseArray<Bitmap>(VeggieType.values().length);

        for (VeggieType t : VeggieType.values()) {
            bitmapImages.put(t.getResourceId(), BitmapFactory.decodeResource(r, t.getResourceId(), new BitmapFactory.Options()));
        }
    }

    public void setWidthAndHeight(int width, int height) {
        this.maxWidth = width;
        this.maxHeight = height;
        this.clip = new Region(0, 0, width, height);
    }

    private Veggie createNewVeggie() {
        int angle = random.nextInt(20) + 70;
        int speed = random.nextInt(50) + 175;
        boolean movingLeft = random.nextBoolean();

        float gravity = random.nextInt(7) + 20.0f;
        float rotationStartingAngle = random.nextInt(360);
        float rotationIncrement = random.nextInt(100) / 10.0f;

        if (random.nextInt(1) % 2 == 0) {
            rotationIncrement *= -1;
        }

        // scale up 2x
        Bitmap randBitmap  = bitmapImages.get(VeggieType.randomVeg().getResourceId());
        randBitmap = Bitmap.createScaledBitmap(randBitmap, randBitmap.getWidth() * 2, randBitmap.getHeight() * 2, false);

        return new Veggie(
                randBitmap, maxWidth, maxHeight,
                angle, speed, gravity,
                movingLeft, rotationIncrement, rotationStartingAngle);
    }

    public int testForCollisions(List<DrawPath> allPaths) {
        int score = 0;
        for (DrawPath p : allPaths) {
            for (Veggie veg : veggies) {
                if(!veg.isSplit()) {
                    // uses regions and path to check for intersection
                    Region projectile = new Region(veg.getLocation());
                    Region path = new Region();
                    path.setPath(p, clip);

                    if (!projectile.quickReject(path) && projectile.op(path, Region.Op.INTERSECT)) {
                        veg.slice();
                        score++;
                    }
                }
            }
        }
        return score;
    }

    public void draw(Canvas canvas) {
        for (Veggie veg : veggies) {
            veg.draw(canvas);
        }
    }

    public void update() {

        if (maxWidth < 0 || maxHeight < 0) {
            return;
        }

        if (random.nextInt(1000) <= 30) {
            veggies.add(createNewVeggie());
        }

        for (Iterator<Veggie> iter = veggies.iterator(); iter.hasNext();) {

            Veggie veg = iter.next();
            veg.move();

            if (veg.movedOffScreen()) {
                iter.remove();
            }
        }
    }

    public void clear(){
        veggies.clear();
    }



}

package com.cs441_fruitcutter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Looper;
import android.support.v4.util.SparseArrayCompat;
import android.util.Log;
import android.view.SurfaceHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import android.os.Handler;

/**
 * Created by Thomas on 2/24/2016.
 */
public class GameThread implements Runnable {

    private static final String TAG = "CS441-FruitCutter";

    // used to draw score
    private final Paint scorePaint = new Paint();
    // used to change the canvas
    private final SurfaceHolder surfaceHolder;
    private Context context = null;
    // used for countdown
    private final GameTimer timer = new GameTimer();
    private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
    private volatile ScheduledFuture<?> self;
    // used to draw paths
    private final Paint linePaint = new Paint();
    private final Paint linePaintBlur = new Paint();
    private SparseArrayCompat<DrawPath> paths;
    // game parameters
    private int score = 0;
    private int width = 0;
    private int height = 0;
    private boolean isRunning = false;
    private VeggieGenerator veggieGenerator = null;

    public GameThread(SurfaceHolder surfaceHolder, Context context, VeggieGenerator veggieGenerator) {
        this.surfaceHolder = surfaceHolder;
        this.context = context;
        this.veggieGenerator = veggieGenerator;
    }

    public void pauseGame() {
        isRunning = false;
        timer.pauseGame();
    }

    public void resumeGame(int width, int height) {
        this.width = width;
        this.height = height;
        isRunning = true;
        timer.resumeGame();
        veggieGenerator.setWidthAndHeight(width, height);
    }

    public void startGame(int width, int height) {
        Log.i(TAG, "startGame");
        // initialize dimensions and paint attributes
        score = 0;
        veggieGenerator.clear();

        this.width = width;
        this.height = height;
        this.isRunning = true;
        this.veggieGenerator.setWidthAndHeight(width, height);
        this.timer.startGame();
        this.self = executor.scheduleAtFixedRate(this, 0, 10, TimeUnit.MILLISECONDS);

        this.scorePaint.setColor(Color.YELLOW);
        this.scorePaint.setAntiAlias(true);
        this.scorePaint.setTextSize(75.0f);
        this.linePaint.setAntiAlias(true);
        this.linePaint.setColor(Color.YELLOW);
        this.linePaint.setStyle(Paint.Style.STROKE);
        this.linePaint.setStrokeJoin(Paint.Join.ROUND);
        this.linePaint.setStrokeWidth(5.0f);
        this.linePaintBlur.set(this.linePaint);
        this.linePaintBlur.setMaskFilter(new BlurMaskFilter(9.0f, BlurMaskFilter.Blur.NORMAL));
    }

    @Override
    public void run() {
        Canvas canvas = null;
        if (isRunning) {
            try {
                if (timer.isGameComplete()) {
                    // game over
                    isRunning = false;
                    self.cancel(true);

                    // get the canvas
                    canvas = surfaceHolder.lockCanvas();

                    // draw background
                    canvas.drawARGB(255, 0, 0, 0);
                    Bitmap background = BitmapFactory.decodeResource(context.getResources(), R.mipmap.background, new BitmapFactory.Options());
                    background = Bitmap.createScaledBitmap(background, width, height, false);
                    canvas.drawBitmap(background, new Matrix(), new Paint());

                    // draw game over and score
                    Handler h = new Handler(Looper.getMainLooper());
                    h.post(new Runnable() {
                        public void run() {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle("Game Over");
                            builder.setMessage("Score: " + score);
                            builder.setCancelable(true);
                            builder.setNeutralButton(android.R.string.ok,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            startGame(width, height);
                                        }
                                    });

                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    });


                } else {
                    // game running, update next frame
                    veggieGenerator.update();

                    // drawn paths
                    if (paths != null && paths.size() > 0) {
                        List<DrawPath> allPaths = new ArrayList<DrawPath>();
                        for (int i = 0; i < paths.size(); i++) {
                            allPaths.add(paths.valueAt(i));
                        }
                        // collision between veggie and drawn path
                        score += veggieGenerator.testForCollisions(allPaths);
                    }
                    // get the canvas
                    canvas = surfaceHolder.lockCanvas();

                    if (canvas != null) {
                        synchronized (surfaceHolder) {
                            // draw background
                            canvas.drawARGB(255, 0, 0, 0);
                            Bitmap background = BitmapFactory.decodeResource(context.getResources(), R.mipmap.background, new BitmapFactory.Options());
                            background = Bitmap.createScaledBitmap(background, width, height, false);
                            canvas.drawBitmap(background, new Matrix(), new Paint());

                            // draw veggies
                            veggieGenerator.draw(canvas);

                            // draw timer and score
                            timer.draw(canvas);
                            canvas.drawText("Score: " + score, width - 500, 60, scorePaint);

                            // draw paths
                            if (paths != null) {
                                for (int i = 0; i < paths.size(); i++) {
                                    canvas.drawPath(paths.valueAt(i), linePaintBlur);
                                    canvas.drawPath(paths.valueAt(i), linePaint);

                                    if (paths.valueAt(i).getTimeDrawn() + 500 < System.currentTimeMillis()) {
                                        paths.removeAt(i);
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            } finally {
                if (canvas != null) {
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }

    }

    public void updateDrawnPath(SparseArrayCompat<DrawPath> paths) {
        this.paths = paths;
    }
}


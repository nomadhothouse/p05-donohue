package com.cs441_fruitcutter;

import android.content.Context;
import android.support.v4.util.SparseArrayCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

/**
 * Created by Thomas on 2/24/2016.
 */
public class GameScene extends SurfaceView implements View.OnTouchListener, SurfaceHolder.Callback {

    private static final String TAG = "CS441-FruitCutter";

    private GameThread gameThread;
    private boolean isGameInitialized = false;
    // http://developer.android.com/reference/android/support/v4/util/SparseArrayCompat.html
    private final SparseArrayCompat<DrawPath> paths = new SparseArrayCompat<DrawPath>();

    private VeggieGenerator veggieGenerator;

    public GameScene(Context context) {
        super(context);

        init();
    }

    public GameScene(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public GameScene(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init();
    }

    private void init() {
        Log.i(TAG, "init");
        this.setOnTouchListener(this);
        this.setFocusable(true);
        this.getHolder().addCallback(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                createNewPath(event.getX(), event.getY(), event.getPointerId(0));
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                int newPointerIndex = event.getActionIndex();
                createNewPath(event.getX(newPointerIndex), event.getY(newPointerIndex), event.getPointerId(newPointerIndex));

                break;
            case MotionEvent.ACTION_MOVE:
                for (int i = 0; i < paths.size(); i++) {
                    int pointerIndex = event.findPointerIndex(paths.indexOfKey(i));

                    if (pointerIndex >= 0) {
                        paths.valueAt(i).lineTo(event.getX(pointerIndex), event.getY(pointerIndex));
                        paths.valueAt(i).updateTimeDrawn(System.currentTimeMillis());
                    }
                }
                break;
        }

        gameThread.updateDrawnPath(paths);
        return true;
    }

    private void createNewPath(float x, float y, int ptrId) {
        DrawPath path = new DrawPath();
        path.moveTo(x, y);
        path.updateTimeDrawn(System.currentTimeMillis());
        paths.append(ptrId, path);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (isGameInitialized) {
            gameThread.resumeGame(width, height);
        } else {
            isGameInitialized = true;
            veggieGenerator = new VeggieGenerator(getResources());
            gameThread = new GameThread(getHolder(), getContext(), veggieGenerator);
            gameThread.startGame(width, height);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.i(TAG, "surfaceCreated");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        gameThread.pauseGame();
    }

}



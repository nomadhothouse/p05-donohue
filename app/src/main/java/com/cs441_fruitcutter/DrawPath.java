package com.cs441_fruitcutter;

import android.graphics.Path;

/**
 * Created by Thomas on 2/25/2016.
 */

// http://developer.android.com/reference/android/graphics/Path.html

public class DrawPath extends Path {
    private long timeDrawn;

    public long getTimeDrawn() {
        return timeDrawn;
    }

    public void updateTimeDrawn(long timeDrawn) {
        this.timeDrawn = timeDrawn;
    }
}

package com.cs441_fruitcutter;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // create the game
        // need FragmentTransaction since it's a Fragment
        GameFragment gameFragment = new GameFragment();
        FragmentTransaction transaction = (FragmentTransaction) getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, gameFragment, "Game");
        transaction.addToBackStack("Game");
        transaction.commit();
    }

}

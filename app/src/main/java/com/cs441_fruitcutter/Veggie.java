package com.cs441_fruitcutter;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by Thomas on 2/27/2016.
 */
public class Veggie {

    // the original image
    private final Bitmap image;

    // image for two halves
    private final Bitmap[] splitVeggie = new Bitmap[2];

    // need these for canvas drawBitmap
    private final Paint paint = new Paint();
    private final Matrix m = new Matrix();

    // motion variables
    private final float rotationIncrement;
    private float gravity;
    private final int maxWidth;
    private final int maxHeight;
    private final int angle;
    private final int initialSpeed;

    private int xLocation;
    private int yLocation;
    private int absYLocation;
    private float rotationAngle;
    private float time = 0.0f;
    private float fallingVelocity = 5.0f;
    private boolean movingLeft;

    private final float moveSpeed = 0.25f;

    // check if split
    private boolean isSplit = false;

    public Veggie(Bitmap image, int maxWidth, int maxHeight, int angle, int initialSpeed, float gravity, boolean movingLeft, float rotationIncrement, float rotationStartingAngle) {
        this.image = image;
        this.maxHeight = maxHeight;
        this.angle = angle;
        this.initialSpeed = initialSpeed;
        this.gravity = gravity;
        this.maxWidth = maxWidth;
        this.movingLeft = movingLeft;
        this.rotationIncrement = rotationIncrement;
        this.rotationAngle = rotationStartingAngle;

        paint.setAntiAlias(true);
    }

    public boolean movedOffScreen() {
        // veggies can exit to the side or bottom of the screen
        return  yLocation < 0 ||
                xLocation + image.getWidth() < 0 ||
                xLocation > maxWidth;
    }

    public void move() {
        if (!isSplit) {
            // move the single piece
            // cos for x direction multiplied by time
            xLocation = (int) (initialSpeed * Math.cos(angle * Math.PI / 180) * time);
            // sin for y direction multiple by time and subtract by half gravity value multiplied by time squared
            yLocation = (int) (initialSpeed * Math.sin(angle * Math.PI / 180) * time - 0.5 * gravity * time * time);

            if (movingLeft) {
                // move the veggie from right to left
                xLocation = maxWidth - image.getWidth() - xLocation;
            }
        } else {
            // move the two split halves down
            yLocation -= time * (fallingVelocity + time * gravity / 2);
            fallingVelocity += time * gravity;
        }

        // 0,0 is top left, parabola should go the other way up
        absYLocation = (yLocation * -1) + maxHeight;

        time += moveSpeed;
    }

    public void draw(Canvas canvas) {

        if (!isSplit) {
            // rotate and translate the single piece
            m.reset();
            m.postTranslate(-image.getWidth() / 2, -image.getHeight() / 2);
            m.postRotate(rotationAngle);
            m.postTranslate(xLocation + (image.getWidth() / 2), absYLocation + (image.getHeight() / 2));
            rotationAngle += rotationIncrement;

            canvas.drawBitmap(image, m, paint);
        } else {
            // draw the split halves
            canvas.drawBitmap(splitVeggie[0], xLocation, absYLocation, paint);
            canvas.drawBitmap(splitVeggie[1], xLocation + image.getWidth() / 2 + 5, absYLocation, paint);
        }
    }

    public Rect getLocation() {
        return new Rect(xLocation, absYLocation, xLocation + image.getWidth(), absYLocation + image.getHeight());
    }

    public void slice() {
        // make gravity faster
        this.gravity /= 12.0f;
        this.time = 0.0f;
        this.isSplit = true;

        // split into two halves
        splitVeggie[0] = Bitmap.createBitmap(image, 0, 0, image.getWidth() / 2, image.getHeight(), m, true);
        splitVeggie[1] = Bitmap.createBitmap(image, image.getWidth() / 2, 0, image.getWidth() / 2, image.getHeight(), m, true);
    }

    public boolean isSplit()
    {
        return isSplit;
    }
}
